<?php

namespace FrancescoASessa\Utils;

class ArrayUtil 
{
    /** Filters out the elements of an array, that have one of the specified values. */
    public static function without(array $items, ...$params) : array
    {
        return array_values(array_diff($items, $params));
    }

    /** Returns the last element in an array. */
    public static function last(array $items) : mixed
    {
        return end($items);
    }

    /**
     * Retrieves all of the values for a given key:
     */
    public static function pluck(array $items,string $key) : array
    {
        return array_map( function($item) use ($key) {
            return is_object($item) ? $item->$key : $item[$key];
        }, $items);
    }

    /** Deep flattens an array. */
    public static function deepFlatten(array $items) : array
    {
        $result = [];
        foreach ($items as $item) {
            if (!is_array($item)) {
                $result[] = $item;
            } else {
                $result = array_merge($result, deepFlatten($item));
            }
        }
    
        return $result;
    }

    /** Sorts a collection of arrays or objects by key. */
    public static function orderBy(array $items, string $attr, string $order = 'desc') : array
    {
        $sortedItems = [];
        foreach ($items as $item) {
            $key = is_object($item) ? $item->{$attr} : $item[$attr];
            $sortedItems[$key] = $item;
        }
        if ($order === 'desc') {
            krsort($sortedItems);
        } else {
            ksort($sortedItems);
        }
    
        return array_values($sortedItems);
    }

    /** Returns true if the provided function returns true for all elements of an array, false otherwise. */
    public static function all(array $items, callable $func) : bool
    {
      return count(array_filter($items, $func)) === count($items);
    }
}
